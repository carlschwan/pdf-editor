// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

#pragma once

#include <QAbstractListModel>
#include <QImage>
#include <QtQml>
#include <poppler-qt6.h>

class PdfPageModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

public:
    enum ExtraRoles {
        PageRole = Qt::UserRole + 1,
        ImageRole,
        RotationRole,
    };
    explicit PdfPageModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = {}) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void loadPdf(const QUrl &filePath);
    Q_INVOKABLE void rotate(int index, int angle);

private:
    QString m_filePath;

    struct Page {
        std::unique_ptr<Poppler::Page> page;
        Poppler::Document *document;
        Poppler::Page::Rotation angle = Poppler::Page::Rotate0;
    };

    std::vector<Page> m_pages;
    std::vector<std::unique_ptr<Poppler::Document>> m_documents;
};