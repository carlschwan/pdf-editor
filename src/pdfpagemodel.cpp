// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

#include "pdfpagemodel.h"

using namespace Qt::StringLiterals;

PdfPageModel::PdfPageModel(QObject *parent)
    : QAbstractListModel(parent)
{
    loadPdf(QUrl::fromLocalFile(u"/home/carl/Documents/interrail.pdf"_s));
}

int PdfPageModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_pages.size();
}

QVariant PdfPageModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid));

    const int row = index.row();
    const Page &page = m_pages[row];

    switch (role) {
    case PageRole:
        return row;
    case RotationRole:
        return page.angle;
    case ImageRole:
        return QVariant::fromValue<QImage>(page.page->renderToImage(140, 140, -1, -1, -1, -1, page.angle));
    default:
        return {};
    }
}

QHash<int, QByteArray> PdfPageModel::roleNames() const
{
    return {
        {PageRole, "page"},
        {ImageRole, "image"},
        {RotationRole, "rotation"},
    };
}

void PdfPageModel::loadPdf(const QUrl &filePath)
{
    beginResetModel();
    m_filePath = filePath.toLocalFile();
    m_pages.clear();
    m_documents.clear();

    auto document = Poppler::Document::load(filePath.toLocalFile());
    document->setRenderBackend(Poppler::Document::SplashBackend);
    if (!document || document->isLocked()) {
        qWarning() << "Failed to load or the document is locked:" << filePath;
        endResetModel();
        return;
    }

    for (int i = 0; i < document->numPages(); ++i) {
        Page page;
        page.document = document.get();
        page.page = document->page(i);
        m_pages.push_back(std::move(page));
    }

    m_documents.push_back(std::move(document));

    endResetModel();
}

void PdfPageModel::rotate(int idx, int _angle)
{
    auto angle = (int)m_pages[idx].angle + _angle;
    if (angle == 360) {
        angle = 0;
    }
    if (angle == -90) {
        angle = 270;
    }
    m_pages[idx].angle = static_cast<Poppler::Page::Rotation>(angle);
    Q_EMIT dataChanged(index(idx, 0), index(idx, 0), {RotationRole});
}
