// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Dialogs
import QtQuick.Controls as Controls
import QtQml.Models
import org.kde.kirigami as Kirigami
import org.kde.kquickcontrolsaddons
import org.kde.kirigamiaddons.delegates as Delegates
import org.kde.pdfeditor

Kirigami.ScrollablePage {
    id: root

    Controls.Menu {
        id: menu

        property int index: -1

        Kirigami.Action {
            text: i18nc("@action", "Rotate Left")
            onTriggered: pdfPageModel.rotate(menu.index, -90)
        }

        Kirigami.Action {
            text: i18nc("@action", "Rotate Right")
            onTriggered: pdfPageModel.rotate(menu.index, 90)
        }
    }

    GridView {
        id: gridView

        cellWidth: Math.floor(width/Math.floor(width/(Kirigami.Units.iconSizes.enormous * 2 + Kirigami.Units.largeSpacing * 2)))
        cellHeight: Kirigami.Units.iconSizes.enormous * 2 + Kirigami.Units.largeSpacing * 2

        displaced: Transition {
            NumberAnimation {
                properties: "x,y"
                easing.type: Easing.OutQuad
            }
        }

        model: DelegateModel {
            id: visualModel

            model: PdfPageModel {
                id: pdfPageModel
            }

            delegate: DropArea {
                id: delegate

                required property int index
                required property var image
                required property int rotation

                property int visualIndex: DelegateModel.itemsIndex

                width: GridView.view.cellWidth
                height: GridView.view.cellHeight

                onEntered: function(drag) {
                    visualModel.items.move((drag.source as Delegates.RoundedItemDelegate).visualIndex, itemDelegate.visualIndex)
                }

                Delegates.RoundedItemDelegate {
                    id: itemDelegate

                    width: image.paintedWidth + Kirigami.Units.largeSpacing * 2
                    height: image.paintedHeight + Kirigami.Units.largeSpacing * 2 + textLabel.height

                    anchors.horizontalCenter: parent.horizontalCenter

                    highlighted: true

                    onClicked: {
                        menu.index = delegate.index;
                        menu.popup();
                    }

                    property int visualIndex: delegate.visualIndex

                    DragHandler {
                        id: dragHandler
                    }

                    Drag.active: dragHandler.active
                    Drag.source: icon
                    Drag.hotSpot.x: width / 2
                    Drag.hotSpot.y: height / 2

                    rotation: delegate.rotation

                    states: [
                        State {
                            when: dragHandler.active
                            ParentChange {
                                target: itemDelegate
                                parent: root
                            }

                            AnchorChanges {
                                target: itemDelegate
                                anchors {
                                    horizontalCenter: undefined
                                }
                            }
                        }
                    ]

                    QImageItem {
                        id: image

                        image: delegate.image
                        width: Kirigami.Units.iconSizes.enormous * 2
                        height: width
                        fillMode: QImageItem.PreserveAspectFit
                        x: Math.round((paintedWidth - width) / 2) + Kirigami.Units.largeSpacing
                        y: Kirigami.Units.largeSpacing
                    }

                    Controls.Label {
                        id: textLabel
                        text: delegate.index + 1
                        anchors {
                            bottom: parent.bottom
                            horizontalCenter: parent.horizontalCenter
                            bottomMargin: Kirigami.Units.smallSpacing
                        }
                    }
                }
            }
        }
    }

    actions: Kirigami.Action {
        text: i18nc("@action:intoolbar", "Add PDF")
        onTriggered: fileDialog.open()
    }

    FileDialog {
        id: fileDialog
        title: i18nc("@title:window", "Select an PDF")
        fileMode: FileDialog.OpenFile
        onAccepted: pdfPageModel.loadPdf(selectedFile)
        nameFilters: [i18n("Pdf files (*.pdf)")]
    }
}
